package main

import (
	"fmt"
	"log"
)

func RomanToInteger(s string) int {
	value := map[byte]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	// add last element
	result := value[s[len(s)-1]]

	for i := 0; i < len(s)-1; i++ {

		left, ok := value[s[i]]
		if !ok {
			log.Println("Invalid Input", s)
			return -1
		}

		right, ok := value[s[i+1]]
		if !ok {
			log.Println("Invalid Input", s)
			return -1
		}

		if left >= right {
			result += left
		} else {
			result -= left
		}
	}
	return result
}

func main() {
	fmt.Println(RomanToInteger("III"))
}
