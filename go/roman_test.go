package main

import "testing"

type TestCase struct {
	roman string
	want  int
}

func TestCommonCaseConvertRomanToInt(t *testing.T) {
	tests := []TestCase{
		{"I", 1},
		{"II", 2},
		{"IV", 4},
		{"V", 5},
		{"IX", 9},
		{"X", 10},
		{"XL", 40},
		{"L", 50},
		{"XC", 90},
		{"C", 100},
		{"CD", 400},
		{"D", 500},
		{"CM", 900},
		{"M", 1000},
		{"MMXXI", 2021},
	}

	for _, tt := range tests {
		t.Run(tt.roman, func(t *testing.T) {
			if got := RomanToInteger(tt.roman); got != tt.want {
				t.Errorf("Decode(%s) = %d, want %d", tt.roman, got, tt.want)
			}
		})
	}
}
func TestShouldReturnNegativeOneWhenInvalidInput(t *testing.T) {
	tests := []TestCase{
		{"ASDFG", -1},
		{"FEWRA", -1},
		{"INVALID", -1},
	}

	for _, tt := range tests {
		t.Run(tt.roman, func(t *testing.T) {
			if got := RomanToInteger(tt.roman); got != tt.want {
				t.Errorf("Decode(%s) = %d, want %d", tt.roman, got, tt.want)
			}
		})
	}
}

