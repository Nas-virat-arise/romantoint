import { RomanToInt } from "./main";

describe("Convert Roman to Int", () => {
  it("should convert Roman to Int", () => {
    expect(RomanToInt("IV")).toEqual(4);
    expect(RomanToInt("MMXXI")).toEqual(2021);
  });
});

describe("Invalid Input", () => {
  it("should return -1 if the string is invalid", () => {
    expect(RomanToInt("UY")).toEqual(NaN);
    expect(RomanToInt("AHDKF")).toEqual(NaN);
    expect(RomanToInt("XIUY")).toEqual(NaN);
    expect(RomanToInt("IVUY")).toEqual(NaN);
  });
});

