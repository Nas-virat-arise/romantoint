export function RomanToInt(s: string): number {
  const roman = new Map<string, number>([
    ['I', 1], ['V', 5], ['X', 10], ['L', 50],
    ['C', 100], ['D', 500], ['M', 1000]
]);

  let sum = 0;
  let prevValue = roman.get(s[0])!;
  for (let i = 1; i < s.length; i++) {
      const currentValue = roman.get(s[i])!;
      if (currentValue > prevValue) sum -= prevValue;
      else sum += prevValue;
      prevValue = currentValue;
  }
  sum += prevValue;
  return sum;
}